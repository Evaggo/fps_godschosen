﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FireTemplate2 : MonoBehaviour
{

    public float timeDestroy = 3.0f;

    public abstract void Fire();

    
    public abstract void DestroySelf();

    private void Awake()
    {
        Invoke("DestroySelf", timeDestroy);
    }
    
}