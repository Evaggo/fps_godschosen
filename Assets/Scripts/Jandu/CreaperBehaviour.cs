﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class CreaperBehaviour : MonoBehaviour
{
    public enum State { Idle, Patrol, Chase, Explode, Dead };
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
    [SerializeField] SoundPlayer sound;

    public AudioSource damage;
    public AudioSource dead;
    public AudioSource attack;

    private bool isDamage;
    public GameObject explosionenemigo;
    //public int vidas;
    public int currentVidas;
    public GameObject graphics;
    public GameObject audios;

    [Header("Creeper properties")]
    public int life = 5;

    [Header("Target Detection")]
    private float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    private bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;

    [Header("Explosion properties")]
    public float explodeDistance;
    public float explosionRadius;
    public float explosionForce;
    public GameObject explosionPS;

    // Use this for initialization
    void Start()
    {
        currentVidas = life;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        nearNode = true;
        SetIdle();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            case State.Explode:
                Explode();
                break;
            case State.Dead:
                Dead();
                break;
            default:
                break;
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bala")
        {
            damage.Play();
            currentVidas--;
            life = currentVidas;
            Debug.Log("quitando vida");
            isDamage = true;
            anim.SetBool("Damage", isDamage);
            if (life <= 0)
            {
                StartCoroutine(Dead02());
            }
            else if (life > 0) {
                StartCoroutine(VuelveIdle());
            }
        }
        if (other.gameObject.tag == "Player")
        {
            
            StartCoroutine(Attack());
            
        }
    }

    void Idle()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (timeCounter >= timeStopped)
        {
            if (nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (agent.remainingDistance <= 0.2f)
        {
            if (stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if (!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }
        agent.SetDestination(targetTransform.position);

        if (Vector3.Distance(transform.position, targetTransform.position) <= explodeDistance)
        {
            SetExplode();
        }


    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;
        anim.SetBool("walking", false);
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        sound.Play(0, 1);

        state = State.Idle;
    }
    void SetPatrol()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
        anim.SetBool("Walking", true);

        state = State.Patrol;
    }
    void SetChase()
    {
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        anim.SetBool("Walking", true);
        radius = chaseRadius;

        state = State.Chase;
    }
    void SetExplode()
    {
        sound.Play(0, 1);
        agent.isStopped = true;
        anim.SetTrigger("Dead");

        state = State.Explode;
    }
    void SetDead()
    {
        //sound.Play(0, 1);
        Destroy(this.gameObject);
        state = State.Dead;
    }

    void GoNextNode()
    {
        currentNode++;
        if (currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for (int i = 0; i < pathNodes.Length; i++)
        {
            if (Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    public void Explosion()
    {
        Collider[] cols = Physics.OverlapSphere(this.transform.position, explosionRadius);
        foreach (Collider c in cols)
        {
            if (c.attachedRigidbody != null)
            {
                Debug.Log("Mato a lo que sea");
                Debug.Log(c.name);
               
                c.attachedRigidbody.AddExplosionForce(explosionForce, this.transform.position, explosionRadius, 1, ForceMode.Impulse);
            }
        }
        int random = Random.Range(4, 8);
        sound.Play(0, 1);
        GameObject explosion = Instantiate(explosionPS,transform);
        explosion.transform.parent = null;

        SetDead();
    }
    private void OnDrawGizmos()
    {
        Color color = Color.blue;
        if (targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        Debug.Log("Creeper damage");
        if (state == State.Dead) return;
        life -= hit;
        if (life <= 0) Explosion();
    }

    IEnumerator Dead02()
    {
        dead.Play();
        isDamage = false;
        anim.SetBool("Damage", isDamage);
        anim.SetTrigger("Dead");
        Debug.Log("olei arsa");
        yield return new WaitForSeconds(1.5f);
        graphics.SetActive(false);
        audios.SetActive(false);
        Instantiate(explosionenemigo, transform.position, transform.rotation);        
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
        state = State.Dead;

    }

    IEnumerator Attack()
    {
        anim.SetBool("Attack", true);
        attack.Play();
        yield return new WaitForSeconds(0.4f);
        anim.SetBool("Attack", false);
        anim.SetBool("Idle", true);
    }

    IEnumerator VuelveIdle()
    {
        Debug.Log("corrutina daño");
        yield return new WaitForSeconds(1f);
        isDamage = false;
        anim.SetBool("Damage", isDamage);
        anim.SetBool("Idle", true);
    }
}
