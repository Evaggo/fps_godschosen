﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using UnityEngine;

public class PlayerManagement : MonoBehaviour
{
    private Vector2 axis;
    private CharacterController controller;      
    public float speed;
    public Vector3 moveDirection;
    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;

    private Vector3 transformDirection;
    public GameObject mano;
    public GameObject lanzacohetes;

    private int currentLives;
    public GameObject[] lives;

    private int musiquita;
    public AudioSource musik;


    [SerializeField] private FireTemplate2 bullet;
    [SerializeField] private FireTemplate2 rocketBullet;

    void Awake()
    {
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            musik.mute = true;
        }
        else if (musiquita == 0)
        {
            musik.mute = false;
        }
    }

    void Start()
    {
        controller = GetComponent<CharacterController>();
        currentLives = 8;
    }

    void Update()
    {          
        transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;
        if (Input.GetButton("Left shift"))
        {
            speed = 15;
        }
        else {
            speed = 5;
        }

        if (controller.isGrounded && !jump)
        {
            moveDirection.y = Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }


        controller.Move(moveDirection * Time.deltaTime);
    }


    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void Fire()
    {
        Debug.Log("Fire");

        // Instanciar una pelota
        FireTemplate2 pelota = Instantiate(bullet, mano.transform.position, mano.transform.rotation) as FireTemplate2;

        // Ponerla en la posición del player

        // Dispararla
        pelota.Fire();
    }

    public void Fire2()
    {
        Debug.Log("Fire2");

        // Instanciar una pelota
        FireTemplate2 misil = Instantiate(rocketBullet, lanzacohetes.transform.position, lanzacohetes.transform.rotation) as FireTemplate2;

        // Ponerla en la posición del player

        // Dispararla
        misil.Fire();
    }

    public void Jump()
    {
        if (!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }

/*
    if (controller.isGrounded && !jump)
        {
            moveDirection.y = forceToGround;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y* gravityMagnitude * Time.deltaTime;
        }

        transformDirection = axis.x* transform.right + axis.y* transform.forward;

        moveDirection.x = transformDirection.x* speed;
        moveDirection.z = transformDirection.z* speed;

        controller.Move(moveDirection* Time.deltaTime);
*/

    void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            StartCoroutine(menosVidas());           
        }
    }

    IEnumerator menosVidas()
    {
        currentLives--;
        lives[currentLives].SetActive(false);
        if (currentLives <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        yield return new WaitForSeconds(3f);
    }
}
