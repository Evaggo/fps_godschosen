﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class QuestManager : MonoBehaviour
{
    public GameObject[] textos;
    public GameObject[] audios;
    private int i;
    private int musiquita;

    public AudioSource backgroundMusic;

    private void Awake()
    {

        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {            
            backgroundMusic.mute = true;
        }
        else if (musiquita == 0)
        {
            backgroundMusic.mute = false;
        }
        

        i = 0;
        textos[i].SetActive(true);
        audios[i].SetActive(true);
    }

    /*
    private void OnTriggerEnter(Collider talk)
    {
        if (talk.gameObject.tag == "Player")
        {
            PressToTalk.SetActive(true);
            if (Input.GetButtonDown("Interactuar"))
            {
                PressToTalk.SetActive(false);
                textos[i].SetActive(true);
            }
        }
            
    }*/
    public void Next()
    {
        if (i == 5)
        {
            SceneManager.LoadScene("Terreno"); 
        }
        if (musiquita == 1)
        {
            i++;
            textos[i - 1].SetActive(false);
            textos[i].SetActive(true);
            audios[i - 1].SetActive(false);
            audios[i].SetActive(false);
        }
        else if (musiquita == 0)
        {
            i++;
            textos[i - 1].SetActive(false);
            textos[i].SetActive(true);
            audios[i - 1].SetActive(false);
            audios[i].SetActive(true);
        }
    }
}
