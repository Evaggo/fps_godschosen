﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MainMenuManager : MonoBehaviour
{
    
    //public GameObject audios;
    private int musiquita;
    public AudioSource musik;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            musik.mute = true;
        }
        else if (musiquita == 0)
        {
            musik.mute = false;
        }
    }
    
    public void PulsaPlay()
    {
        SceneManager.LoadScene("Prologue");
    }

    public void PulsaRestart()
    {
        SceneManager.LoadScene("Terreno");
    }

    public void PulsaOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }

    
    public void PulsaSoundON()
    {
        musik.mute = false;
        PlayerPrefs.SetInt("Musica", 0);
       
    }

    public void PulsaSoundOFF()
    {
        musik.mute = true;
        PlayerPrefs.SetInt("Musica", 1);
    }
    

    public void PulsaGoBack()
    {
        SceneManager.LoadScene("Menu");
    }
    
}
