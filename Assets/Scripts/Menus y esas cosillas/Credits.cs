﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Credits : MonoBehaviour
{
   private int musiquita;
    public AudioSource backgroundMusic;

    public void Awake()
    {
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            backgroundMusic.mute = true;
        }
        else if (musiquita == 0)
        {
            backgroundMusic.mute = false;
        }
    }
    

    public void GoBack()
    {
        SceneManager.LoadScene("Menu");
    }
}

