﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject canvas;
    public GameObject player;

    public void Continue()
    {
        Debug.Log("continuar");
        isPaused = false;
        canvas.SetActive(false);
        LookRotation fpc1 = player.GetComponent<LookRotation>();
        PlayerManagement fpc2 = player.GetComponent<PlayerManagement>();
        fpc1.enabled = true;
        fpc2.enabled = true;

        Time.timeScale = 1.0f;
    }

    public void Quit()
    {
        Debug.Log("quitar");
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu");
    }

    void Update()
    {
        if (!isPaused && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
        {
            ActivatePause();
        }
        else if (isPaused && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
        {
            Continue();
        }
        else if (isPaused && (Input.GetKeyDown(KeyCode.Q)))
        {
            Quit();
        }
    }

    void ActivatePause()
    {
        isPaused = true;
        Time.timeScale = 0;
        canvas.SetActive(true);
        LookRotation fpc1 = player.GetComponent<LookRotation>();
        PlayerManagement fpc2 = player.GetComponent<PlayerManagement>();
        fpc1.enabled = false;
        fpc2.enabled = false;
        /*Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;*/
    }

}