﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SimpleEnemy : MonoBehaviour
{

    public Collider[] patrolPoints;
    private int currentPoint = 0;
    // Use this for initialization
    private NavMeshAgent agent;
    public int vidas;
    public int currentVidas;
    public GameObject graphics;
    public ParticleSystem respawnParticles;
    public ParticleSystem muerteParticles;
    [SerializeField] private Animator animator;

    private bool isIdle;
    private bool isAttack;
    private bool isDamage;
    public GameObject explosionenemigo;

    //Target Detection
    [Header("Target Detection")]
    private float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    private bool targetDetected = false;
    private Transform targetTransform;

    //[SerializeField] Collider collider;


    void Start()
    {
        currentVidas = vidas;
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
        isAttack = false;
        isDamage = false;
    }

    void Update()
    {
        agent.SetDestination(targetTransform.position);
    }

    void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }


    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PatrolPoint")
        { 
            currentPoint++;
            currentPoint = currentPoint % patrolPoints.Length;
            agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
        }
       
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bala")
        {
            currentVidas--;
            vidas = currentVidas;
            Debug.Log("quitando vida");
            isDamage = true;
            animator.SetBool("Damage", isDamage);
            if (vidas <= 0)
            {
                StartCoroutine(Dead());
            }
            else if (vidas > 0) {
                Debug.Log("haciendo daño");
                StartCoroutine(Idle());
            }
        }
    }


    IEnumerator Idle()
    {
        Debug.Log("corrutina daño");
        yield return new WaitForSeconds(1f);
        isDamage = false;
        animator.SetBool("Damage", isDamage);
        isIdle = true;
        animator.SetBool("Idle", isIdle);

        if (targetDetected)
        {
            StartCoroutine(SetChase());
        }
    }

    IEnumerator Dead()
    {
        isDamage = false;
        animator.SetBool("Damage", isDamage);
        animator.SetTrigger("Dead");
        Debug.Log("olei arsa");
        //muerteParticles.Play();
        yield return new WaitForSeconds(1.5f);
        graphics.SetActive(false);
        Instantiate(explosionenemigo, transform.position, transform.rotation);
        //collider.enabled = false;
        yield return new WaitForSeconds(5f);
        currentVidas = vidas;
        //respawnParticles.Play();
        //graphics.SetActive(true);
        //collider.enabled = true;
    }

    IEnumerator SetChase()
    {
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;        
        radius = chaseRadius;
        yield return new WaitForSeconds(0f);
    }

}
