﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject monstruo;
    public float timeLaunchFormation;
    private float currentTime = 0;
    public new Vector3 Spawner;
    [SerializeField] ParticleSystem ps;

    void Awake()
    {
        //ps.transform.position = Spawner;
        StartCoroutine(LanzaFormacion());
        StartCoroutine(Particulas());
    }

    IEnumerator LanzaFormacion()
    {
        while (true)
        {
            GameObject monstruoSpawn;
            // ParticleSystem particulass;
            //  particulass = Instantiate(ps, Spawner, Quaternion.identity);
            ps.Play();
            monstruoSpawn = Instantiate(monstruo, Spawner, Quaternion.identity);

            //Instantiate(monstruo, new Vector3(Random.Range(150, 180)), 6, Random.Range(40, 130), Quaternion.identity, this.GameObject);
            yield return new WaitForSeconds(timeLaunchFormation);
        }
    }

        IEnumerator Particulas()
        {
            while (true)
            {
                
                ParticleSystem particulass;
                particulass = Instantiate(ps, Spawner, Quaternion.identity);
                ps.Play();
                yield return new WaitForSeconds(timeLaunchFormation);
            }
        }
}
